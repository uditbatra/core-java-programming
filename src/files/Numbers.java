/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// PROGRAM NAME : Numbers.java
// PROGRAM DESC : This is a program to print Numbers
// PROGRAM DATE : 27-Nov-2019
// AUTHOR 		: Udit Batra
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

package com.programs;

public class Numbers {
	public static void main(String[] args) {
		
		int numbers[]= {1,2,3,4,5,6,7,8,9,10};
		
		// Print out all the numbers
		for(int number : numbers) {
			System.out.println("The number is : " + number);
		}		
	}
}
